import { useRoutes, Navigate } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";
import Overview from "../pages/Overview";
import User from "../pages/User";
import Staffsale from "../pages/Staffsale";
import Totalfee from "../pages/Totalfee";
import Setting from "../pages/Setting";
import Login from "../pages/Login";
import Notfound from "../pages/Notfound";
import Loanslist from "../pages/LoansList";


const Router = (): JSX.Element | null => {
  const element = useRoutes([
    {
      path: "/",
      element: <MainLayout />,
      children: [
        {
          path: "/",
          element: <Overview />,
        },
        {
          path: "/user",
          element: <User />,
        },
        {
          path: "/staff-sale",
          element: <Staffsale />,
        },
        {
          path: "/loans-list",
          element: <Loanslist />,
        },
        {
          path: "/total-fee",
          element: <Totalfee />,
        },
        {
          path: "/setting",
          element: <Setting />,
        }
      ],
    },
    {
      path: "login",
      element: <Login />,
    },
    {
      path: "*",
      element: <Navigate to="/404" />,
    },
    {
      path: "/404",
      element: <Notfound />,
    },
  ]);
  return element;
};
export default Router;
